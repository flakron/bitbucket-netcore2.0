﻿using System.Linq;
using API.Controllers;
using Xunit;

namespace API.Tests
{
    public class ValuesControllerTests
    {
        [Fact]
        public void Get_Returns()
        {
            // Prepare
            var controller = new ValuesController();
            
            // Act
            var result = controller.Get();

            // Assert
            Assert.NotNull(result);
            
            Assert.Contains("value1", result.ToList());
            Assert.Contains("value2", result.ToList());
        }
    }
}